function addResults(result) {
  var tmp = JSON.parse(result);
  alert(tmp);
}

$("select").change(function () {
  var option=$("select").find(":selected").text();
  console.log(option);
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "https://billgatesbooks.wl.r.appspot.com/" + option,
    success: function (result) {
      
      var table="<th>Image</th><th>Title</th><th>Author</th><th>Abstract</th>";
      
      for(var i=0; i<result[option].length;++i) {
        table += "<tr>" + 
         "<td><img src='" + result[option][i].image + "' width='256px'></td>" + 
          "<td>" + result[option][i].title + "</td>" +
        "<td width=200px>" + result[option][i].author + "</td>" +
        "<td width=700px>" + result[option][i].abstract + "</td>" +
          "</tr>"; 
      }
      
      //
      $("#results").html("<table>" + table + "</table>");
    },
    error: function (e) {
      console.log("Error!");
      console.log(e);
    }
  });
});